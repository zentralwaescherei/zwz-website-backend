module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    'security',
    'prettier',
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:security/recommended',
    'prettier',
    'plugin:prettier/recommended'
  ],
  rules:
  {
    'no-console': ['error'],
    'array-element-newline': [
      'error',
      {
       'ArrayExpression': 'consistent',
       'ArrayPattern': { 'minItems': 3 },
      }
    ],
    "object-curly-newline": [
      "error",
      {
        "ObjectExpression": { "multiline": true, "minProperties": 2 },
        "ImportDeclaration": "never",
        "ExportDeclaration": { "multiline": true, "minProperties": 2 }
      }
    ]
  },
};
