import axios from 'axios'
import { TwitchService } from '../services/streaming/twitch.service'
import StartUpError from './startup-error'

export function initTwitchService(): TwitchService {
	const appAccessToken = process.env.TWITCH_APP_ACCESS_TOKEN
	const clientId = process.env.TWITCH_CLIENT_ID
	if (appAccessToken && clientId) {
		return new TwitchService(axios, appAccessToken, clientId)
	}
	throw new StartUpError('Invalid Configuration for TwitchService.')
}
