import { EventsService } from '../services/events/events.service'
import { EventsRepository, PersistableEvent } from '../storage/database/events.storage'
import { getRepository } from 'fireorm'
import CodaIntegrationService from '../services/coda-integration.service'

export default function initEventService(codaIntegrationService: CodaIntegrationService): EventsService {
	return new EventsService(codaIntegrationService, getRepository(PersistableEvent) as EventsRepository)
}
