import * as admin from 'firebase-admin'
import * as fireorm from 'fireorm'
import StartUpError from '../startup/startup-error'
import getLogger from '../logger/logger'

const logger = getLogger({ module: 'firestore.startup' })

export default function initFirestore(): FirebaseFirestore.Firestore {
	try {
		logger.info('Initializing firestore...')
		admin.initializeApp()
		const firestore = admin.firestore()
		firestore.settings({ ignoreUndefinedProperties: true })
		admin.firestore.setLogFunction(getLogger({ module: 'firestore' }).debug)
		fireorm.initialize(firestore)
		logger.info('Firestore initialized.')
		return firestore
	} catch (error) {
		logger.error(error)
		throw new StartUpError('Firestore init failed.')
	}
}
