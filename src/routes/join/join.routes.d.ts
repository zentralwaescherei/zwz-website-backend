export interface JoinRequest {
	name: string
	email: string
	message: string
}
