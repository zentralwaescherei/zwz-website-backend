import express from 'express'
import getLogger from '../../logger/logger'
import { initTwitchService } from '../../startup/streaming.startup'
import { ZwStream } from './streaming.routes.d'

export const streamingBaseUrl = '/api/streaming'

const logger = getLogger({ module: 'events.routes' })

export function initStreaming(): express.Router {
	logger.debug('init streaming routes')
	const router = express.Router()
	router.get('/', async (_, resp) => {
		const zwStream: ZwStream = { isLive: await initTwitchService().checkIfLive() }
		resp.send(zwStream)
	})
	logger.debug('streaming initialized')
	return router
}
