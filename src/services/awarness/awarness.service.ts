import { IncidentTO } from './awarness.service.d'
import { Transporter } from 'nodemailer'
import SMTPTransport from 'nodemailer/lib/smtp-transport'
import { emailSender } from '../../startup/email.startup'
import getLogger from '../../logger/logger'
import ServiceError from '../service-error'

const emailReceiver = process.env.CARE_TEAM_EMAIL_USER
const anonymous = 'anonymous'

const logger = getLogger({ module: 'awarness.service' })

export class AwarnessService {
	constructor(private readonly emailTransporter: Transporter<SMTPTransport.SentMessageInfo>) {}

	async reportIncident(incident: IncidentTO): Promise<string> {
		try {
			const reporterName = incident.name || anonymous
			const reporterEmail = incident.email || anonymous
			const info = await this.emailTransporter.sendMail({
				from: emailSender,
				to: emailReceiver,
				subject: `New Incident reported by ${reporterName} `,
				text: `from: ${reporterName}, email: ${reporterEmail}, message: ${incident.message}`,
				html: `
				from: ${reporterName}</br>
				email: ${reporterEmail}</br>
				message: ${incident.message}`,
			})
			logger.info(`Email sent ${info.messageId}`)
			return info.messageId
		} catch (error) {
			logger.error(error)
			throw new ServiceError('Email sending failed.')
		}
	}
}
